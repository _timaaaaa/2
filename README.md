from abc import ABC, abstractmethod

class Figure(ABC):
    def __init__(self, name):
        self.name = name

    def area(self):
        pass

    def perimetr(self):
        pass

    def is_figure_name(name):
        if name in ['rectangle']:
            return True
        return False

    def __str__(self):
        return '%s with %s perimetr and %s area' % (self.name, self.perimetr(), self.area())

class Rectangle(Figure):
    def __init__(self, a, b, name='reactangle'):
        super().__init__(name)
        self.a = a
        self.b = b

    def area(self):
        return self.a * self.b

    def perimetr(self):
        return 2 * (self.a + self.b)

a = Rectangle(10, 10)
print(a)
